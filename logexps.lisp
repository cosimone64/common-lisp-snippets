;;;; Copyright (C) 2020-2021 cosimone64

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public License
;;;; as published by the Free Software Foundation, either version 3 of
;;;; the License, or (at your option) any later version.

;;;; You should have received a copy of the GNU AGPLv3 with this software,
;;;; if not, please visit <https://www.gnu.org/licenses/>

(ql:quickload '(:alexandria :infix-math :cl-ppcre))

(defpackage :logexps
  (:use :common-lisp)
  (:export :expression-equalp)
  (:import-from :alexandria :flatten)
  (:import-from :infix-math :$)
  (:import-from :cl-ppcre :split))

(in-package :logexps)

;; Defined as a macro to exploit OR lazy evaluation.
(defmacro implies (a b)
  `(or (not ,a) ,b))

(defmacro pipe (arg &rest functions)
  "Process ARG through FUNCTIONS, applying each function in the order they are
written.  If the first function in FUNCTIONS takes more than one argument, ARG
must be a list of its arguments.  Otherwise, it can be a single element, without
having to be in a list."
  (let ((pipeline-function `(apply #'compose (reverse (list ,@functions)))))
    (if (consp arg)
        `(apply ,pipeline-function (list ,@arg))
        `(funcall ,pipeline-function ,arg))))

(defun replace-symbol-value (symbols symbol value)
  (cond ((null symbols) symbols)
        ((atom symbols) (if (eql symbols symbol) value symbols))
        (t (let ((head (first symbols))
                 (tail (rest symbols)))
             (cons (cond ((listp head)
                          (replace-symbol-value head symbol value))
                         ((eql head symbol) value)
                         (t head))
                   (replace-symbol-value tail symbol value))))))

(defun gather-variables (logexp)
  "Return a sorted list of all the logical variables contained in LOGEXP."
  (sort (remove-if (lambda (x) (or (eql x 'or) (eql x 'and) (eql x 'not)))
                   (flatten logexp))
        (lambda (a b) (string< (string a) (string b)))))

;; Is this function necessary?
(defun get-binary-digits (n)
  "Return list of the binary digits of the pure binary representation of N."
  (map 'list #'digit-char-p (let ((*print-base* 2))
                              (prin1-to-string n))))

(defun possible-truth-assignments (symbol-num)
  "Given SYMBOL-NUM, return a list of all possible truth assignments.
The list is of course going to be 2^SYMBOL-NUM long."
  (flet ((binary-digits (n)
           (let ((binary-string
                  (format nil (concatenate 'string "~"
                                           (prin1-to-string symbol-num)
                                           ",'0b") n)))
             (map 'list #'digit-char-p binary-string))))
    (loop for i below (expt 2 symbol-num)
          for truth-values = (binary-digits i)
          collect (mapcar (lambda (x) (if (zerop x) nil t)) truth-values))))

(defun literal-expression (expression truth-assignment)
  "Given EXPRESSION, replace its variables with the given ASSIGNMENT."
  (let* ((vars (gather-variables expression))
         (length (length vars)))
    (flet ((substitute-value (expr i)
             (replace-symbol-value expr
                                   (elt vars i)
                                   (elt truth-assignment i))))
      (do ((i 0 (1+ i))
           (e (substitute-value expression 0) (substitute-value e i)))
          ((>= i length) e)))))

(defun all-assignments (expression)
  "Given EXPRESSION, return a list containing its value for every assignment"
  (let* ((variables (gather-variables expression))
         (length (length variables))
         (assignments (possible-truth-assignments length)))
    (loop for assignment in assignments
          collect (let ((value (eval (literal-expression expression
                                                         assignment))))
                    (list :value value :assignment assignment)))))

(defun normalize-symbol (symbol)
  "Normalize SYMBOL for the $ infox macro."
  (if (not (find symbol '(and or implies)))
      symbol
      (read-from-string (concatenate 'string "."
                                     (string symbol)
                                     "."))))

;; TODO: define operator priorities!
(defun read-infix-expression (expression)
  (pipe expression
        (lambda (exp) (mapcar #'read-from-string (split " " exp)))
        (lambda (exp) (mapcar #'normalize-symbol exp))
        (lambda (exp) (macroexpand-1 (cons '$ exp)))
        (lambda (exp) (if (= (length exp) 1)
                          (first exp)
                          exp))))

(defun expression-equalp (expression1 expression2)
  (let ((normalized-expression1 (if (typep expression1 'string)
                                    (read-infix-expression expression1)
                                    expression1))
        (normalized-expression2 (if (typep expression2 'string)
                                    (read-infix-expression expression2)
                                    expression2)))
    (when (equal (gather-variables normalized-expression1)
                 (gather-variables normalized-expression2))
      (equal (all-assignments normalized-expression1)
             (all-assignments normalized-expression2)))))
