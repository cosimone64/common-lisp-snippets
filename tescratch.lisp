;;;; Copyright (C) 2020-2021 cosimone64

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public License
;;;; as published by the Free Software Foundation, either version 3 of
;;;; the License, or (at your option) any later version.

;;;; You should have received a copy of the GNU AGPLv3 with this software,
;;;; if not, please visit <https://www.gnu.org/licenses/>

(ql:quickload :alexandria)

(defconstant +dt+ 0.001)

(defun prob (freq)
  (let ((dt 0.0001))
    (* freq dt)))

(defun inverse-exponential-distribution (parameter y)
  (- (* (/ parameter) (log (- 1 y)))))

(defun make-inverse-exponential-distribution (parameter)
  (lambda (y) (inverse-exponential-distribution parameter y)))

(defun random-list (length random-generator)
  (symbol-macrolet ((next-number (funcall random-generator)))
    (loop repeat length
          for point = next-number then (+ point next-number)
          collect point)))

(defun poisson-points (length parameter)
  (random-list length (lambda ()
                        (inverse-exponential-distribution parameter
                                                          (random 1.0)))))


(defun make-mm1-queue (arrival-feq service-freq &optional (customers 0))
  (when (>= arrival-feq service-freq)
    (print "The queue is not ergodic, customers will grow indefinitely."))
  (lambda (op) (ecase op
                 (:tick (setf customers (+ customers (prob arrival-feq)
                                           (if (zerop customers)
                                               0
                                               (- (prob service-freq))))))
                 (:get-n customers))))

(defclass mmc-queue ()
  ((users-in-queue :accessor n :initform 0
                   :documentation "Number of users in the queue")
   (arrival-frequency :reader arrival-freq
                      :initform 1
                      :initarg :arrival-freq
                      :documentation "Arrival frequency")
   (service-frequency :reader service-freq
                      :initform 2
                      :initarg :service-freq
                      :documentation "Service frequency")
   (number-of-servers :reader c
                      :initform 1
                      :initarg :server-num
                      :documentation "Number of servers in the queue."))
  (:documentation "An M/M/c queue."))

(defgeneric ergodicp (queue))
(defgeneric tick (queue dt))

(defmethod print-object ((queue mmc-queue) stream)
  (print-unreadable-object (queue stream :type t)
    (with-accessors ((n n) (a arrival-freq) (s service-freq) (c c)) queue
      (format stream
              ":users-in-queue ~s :arrival-freq ~s :service-freq ~d :server-num ~d"
              n a s c))))

(defmethod ergodicp ((queue mmc-queue))
  (< (arrival-freq queue) (* (c queue) (service-freq queue))))

(defun probabilistic-success (probability)
  "Return T with probability PROBABILITY, NIL otherwise."
  (< (random 1.0) probability))

(defmethod tick ((queue mmc-queue) dt)
  (let ((arrival-prob (* dt (arrival-freq queue)))
        (service-freq (* dt
                         (service-freq queue)
                         (min (c queue) (n queue)))))
    (when (probabilistic-success arrival-prob)
      (incf (n queue)))
    (when (and (not (zerop (n queue)))
               (probabilistic-success service-freq))
      (decf (n queue)))))

(defmethod tick-for-period ((queue mmc-queue) seconds &optional real-time)
  (do ((dt 1/1000)
       (time 0 (+ time dt)))
      ((>= time seconds))
    (tick-mm1-queue queue dt)
    (when real-time
      (sleep dt))))

(defun mean-mm1-users-formula (queue)
  "Compute mean number of customers in an ergodic M/M/1 QUEUE.
Use the closed form formula."
  (when (not (ergodicp queue))
    (error "QUEUE is not ergodic. Cannot use closed form formula."))
  (when (/= (c queue) 1)
    (error "QUEUE has more than one server."))
  (let ((rho (/ (arrival-freq queue) (service-freq queue))))
    (/ rho (- 1 rho))))

(defmethod mean-mmc-users-simulation ((queue mmc-queue) &optional real-time)
  "Compute mean number of customers in an M/M/1 QUEUE.
This works also for non-ergodic queues, since the result is obtained via
simulation over a prolonged period of time."
  (do* ((delta 1)
        (simulation-time 3600)
        (time 0 (+ time delta))
        (samples))
       ((>= time simulation-time) (alexandria:mean samples))
    (tick-for-period queue delta real-time)
    (push (n queue) samples)))
